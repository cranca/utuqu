UTUQU
-----
Utuqu es una librería CSS para desarrollo web.

Esta librería parte de algunos prejuicios fuertemente enraizados en mi vida:

  * Solo uso clases
  * Sigo la nomenclatura BEM
  * Documentación en la propia hoja de estilos
  * Los enlaces los subrayo, para que parezcan enlaces...
  * Nada de JavaScript
  * Nombres de las clases en castellano
  * Ni pre-procesadores ni CLI, copipaste del de toda la vida

 ## Próximos capítulos
 
  * Paneles deslizantes
  * Clases para formularios
